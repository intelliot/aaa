FROM openresty/openresty:1.21.4.1-alpine

ENV SESSION_VERSION=2.22
ENV HTTP_VERSION=0.12
ENV OPENIDC_VERSION=1.6.1
ENV JWT_VERSION=0.2.0
ENV HMAC_VERSION=989f601acbe74dee71c1a48f3e140a427f2d03ae

ENV GH=https://github.com
ENV SESSION_REPO=${GH}/bungle/lua-resty-session/archive/v${SESSION_VERSION}.tar.gz
ENV HTTP_REPO=${GH}/pintsized/lua-resty-http/archive/v${HTTP_VERSION}.tar.gz
ENV OPENIDC_REPO=${GH}/pingidentity/lua-resty-openidc/archive/v${OPENIDC_VERSION}.tar.gz
ENV JWT_REPO=${GH}/cdbattags/lua-resty-jwt/archive/v${JWT_VERSION}.tar.gz
ENV HMAC_REPO=${GH}/jkeys089/lua-resty-hmac/archive/${HMAC_VERSION}.tar.gz

ENV UNTAR="tar xz -C /usr/local/openresty/lualib/resty --strip-components=3"

# Path to store nginx key, crt
RUN mkdir -p /etc/nginx/ssl

# Path to store nginx crt pass
RUN mkdir -p /var/lib/nginx

# Custom root certificates path
RUN mkdir -p /usr/local/share/ca-certificates

RUN apk --no-cache add curl

RUN curl -sSL "${SESSION_REPO}" | ${UNTAR} lua-resty-session-${SESSION_VERSION}/lib/resty/
RUN curl -sSL "${HTTP_REPO}"    | ${UNTAR} lua-resty-http-${HTTP_VERSION}/lib/resty/
RUN curl -sSL "${OPENIDC_REPO}" | ${UNTAR} lua-resty-openidc-${OPENIDC_VERSION}/lib/resty/
RUN curl -sSL "${JWT_REPO}"     | ${UNTAR} lua-resty-jwt-${JWT_VERSION}/lib/resty/
RUN curl -sSL "${HMAC_REPO}"    | ${UNTAR} lua-resty-hmac-${HMAC_VERSION}/lib/resty/

COPY bootstrap.sh /usr/local/openresty/bootstrap.sh
COPY nginx /usr/local/openresty/nginx/

ENV AAA_HOST=user179.mhl.tuc.gr
ENV AAA_REALM=uc3
ENV AAA_RESOURCES=hil-service
ENV OID_CLIENT_ID=uc3
ENV OID_CLIENT_SECRET=RvG1JnFzpsqYadLFEnHt5HG7To5c23WA
ENV OID_REDIRECT=/auth
ENV SERVER_NAME=3.65.34.194

# COPY nano1.fritz.box.crt //etc/nginx/ssl/proxy.crt
COPY uc3/aws/3.65.34.194.crt.pem /etc/nginx/ssl/proxy.crt
COPY uc3/aws/3.65.34.194.key /etc/nginx/ssl/proxy.key
COPY root.crt /usr/local/share/ca-certificates/root.crt
COPY ssl_passwords.txt /var/lib/nginx/ssl_passwords.txt
ENTRYPOINT ["/usr/local/openresty/bootstrap.sh"]
