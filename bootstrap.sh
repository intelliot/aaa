#!/bin/sh

# So, for some non-obvious reason nginx with OpenResty does not fetch resolver
# nameserver from /etc/resolv.conf by default. It is not possible to read this
# using Lua when starting Nginx either so we have to patch the config before
# Nginx starts.

set -e
#set -u

# --- TSI ---
chmod -R 600 /etc/nginx/ssl

# Add our own root certificate to the trusted root certificates
cat /usr/local/share/ca-certificates/root.crt >> /etc/ssl/certs/ca-certificates.crt
# --- TSI ---

RESOLV_CONF=/etc/resolv.conf

NGX_DIR=/usr/local/openresty/nginx
NGX_CONF="${NGX_DIR}/conf/nginx.conf"
NGX_SITE="${NGX_DIR}/conf/sites/proxy.conf"

OPENRESTY_BIN_DIR=/usr/local/openresty/bin
OPENRESTY_BIN=${OPENRESTY_BIN_DIR}/openresty

if [ -z "${NAMESERVER}" ] && [ -f "${RESOLV_CONF}" ]; then
    export NAMESERVER=`cat ${RESOLV_CONF} \
                     | grep "nameserver" \
                     | awk '{print $2}' \
                     | tr '\n' ' '`
fi

if [ -n "${NAMESERVER}" ]; then
  echo "Nameserver is: ${NAMESERVER}"

  echo "Patching nginx config"
  sed -i "s/resolver.*/resolver ${NAMESERVER};/" "${NGX_CONF}"
fi

echo "Using nginx config ${NGX_CONF}:"
cat "${NGX_CONF}"
cat "${NGX_SITE}"
printenv
tail -40 /etc/ssl/certs/ca-certificates.crt
/usr/local/openresty/openssl/bin/openssl x509 -in /etc/nginx/ssl/proxy.crt --text
/usr/local/openresty/openssl/bin/openssl x509 -in /usr/local/share/ca-certificates/root.crt --text
echo "Starting nginx:"
"${OPENRESTY_BIN}" -g "daemon off;"
