# Edge Proxy AAA

This repository provides a TLS termination proxy with OAuth2.0/OIDC support.

The underlying proxy is [nginx](https://nginx.org/). OAuth2.0/OIDC support is provided by [OpenResty](https://openresty.org/en/). All tests have been done using [Keycloak](https://www.keycloak.org/) identity and access manager.

Edge Proxy AAA is used to integrate Keycloak with Siemens Edge Nodes (EN) for the "Use Case 3 - Manufacturing" of the [IntellIoT](https://intelliot.eu/) project.

## Build

We use quay.io as docker registry:

    docker login quay.io/intelliot

This repo creates a **base** image. Built the image locally and then push it on `quay.io`:

    docker build -t quay.io/intelliot/edge-proxy-aaa:<version> docker/
    docker push quay.io/intelliot/edge-proxy-aaa:<version>

Useful links:

- [OpenResty Reference](https://openresty-reference.readthedocs.io/en/latest/)

## Integration

The base image is used by each EN with its respective configuration. A EN Dockerfile should be in the form:

```
FROM quay.io/intelliot/edge-proxy-aaa:<version>

ENV AAA_HOST=user179.mhl.tuc.gr
ENV AAA_REALM=uc3
ENV AAA_RESOURCES=hil-service another-service app1
ENV OID_CLIENT_ID=...
ENV OID_CLIENT_SECRET=...
ENV OID_REDIRECT=/auth
ENV SERVER_NAME=<ip or fqdn>

COPY <EN.crt.pem> /etc/nginx/ssl/proxy.crt
COPY <EN.key.pem> /etc/nginx/ssl/proxy.key
COPY ssl_passwords.txt /var/lib/nginx/ssl_passwords.txt
COPY root.crt.pem /usr/local/share/ca-certificates/root.crt
```

`AAA_HOST` and `AAA_REALM` are provided by the Keycloak admin, in this case TSI. The one shown at the example is hosted on our premises.

`AAA_RESOURCES` contains all resources behind the nginx proxy, space separated.

`OID_CLIENT_ID`, `OID_CLIENT_SECRET`, and `OID_REDIRECT` are provided by the Keycloak admin, in this case TSI.

`SERVER_NAME` is the static IP or FQDN of the specific EN.

We also provide the EN with a password protected private key and a signed certificate. The certificate is signed by our own local CA. We also provide the CA root certificate to be added on the EN image as trusted root certificate.

## Local CA

TSI IntellIoT CA.

Certificate needed by clients:

    local-ca/ca/IntellIoT_CA/IntellIoT_CA.crt.pem

Keycloak certificate/key:

    local-ca/server/user179.mhl.tuc.gr/user179.mhl.tuc.gr.key.pem
    local-ca/server/user179.mhl.tuc.gr/user179.mhl.tuc.gr.crt.pem

IED certificate/key:

    local-ca/server/3.65.34.194/3.65.34.194.key
    local-ca/server/3.65.34.194/3.65.34.194.full.crt.pem

More pairs needed, one for each EN used.
