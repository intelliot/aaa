-- OAuth2 support for nginx
--
-- Mandatory enviroment variables:
--
-- AAA_DOMAIN,        e.g. aaa.instance.local:3333
-- AAA_REALM,         e.g. myrealm
-- AAA_RESOURCES,     e.g. app1 app2 app3
-- OID_CLIENT_ID,     e.g. myclient
-- OID_CLIENT_SECRET, e.g. <hex value>
--
-- Optional but needed for our setup:
--
-- OID_REDIRECT = /auth
--
-- Possible return statuses:
--
-- 200 OK,                    user authenticated and authorized to access the requested resource
-- 401 Unauthorized,          user needs to authenticate first
-- 403 Forbidden,             user authenticated but authorization failed
-- 500 Internal Server Error, mandatory vars are missing
--
local utils = require "utils" 

-- AAA Instance Configuration
--
local aaa = {
	scheme    = utils.getEnv("AAA_SCHEME", "https"),
	host      = os.getenv("AAA_HOST"),
	realm     = os.getenv("AAA_REALM"),
	resources = os.getenv("AAA_RESOURCES"),
}

-- Check that mandatory options are set
--
if aaa.host == nil or aaa.realm == nil or aaa.resources == nil then
	ngx.log(ngx.INFO, "All mandatory AAA options must be declared in the environment")
	ngx.exit(ngx.INTERNAL_SERVER_ERROR)
end

-- Default values for resty.openidc configuration
--
-- Used only when no value is provided throuth the environment.
-- All time related options are in seconds.
--
local def = {
	redirect = "/redirect_uri",
	discovery = aaa.scheme.."://"..aaa.host.."/realms/"..aaa.realm.."/.well-known/openid-configuration",
	-- One of:
	--
	-- 1. client_secret_basic
	-- 2. client_secret_post
	-- 3. private_key_basic
	-- 4. client_secret_jwt
	authMethod = "client_secret_basic",
	logoutPath = "/logout",
	--logoutPath = "/edge-proxy/logout",
	logoutHint = true,
	logoutRevokeTokens = true,
	renewAccessTokensOnExpiry = true,
	refreshSessionInterval = 120,
	accessTokenExpiresLeeway = 60,
	iatSlack = 60,
	acceptNoneAlg = false,
	acceptUnsupportedAlg = false,
	useNonce = true,
	usePkce = true,
	responseMode = "form_post",
}

-- Options passed to resty.openidc
--
-- This is not an exhaustive list of all resty.openidc options,
-- just those used in this script. For a complete list of options:
--
-- 	curl -sSL https://raw.githubusercontent.com/zmartzone/lua-resty-openidc/master/lib/resty/openidc.lua | grep -oE "opts\.\w+" | sort | uniq
--
-- All time related options are in seconds.
--
-- Option					EnvVar
-- ------					------
-- client_id					OID_CLIENT_ID
-- client_secret				OID_CLIENT_SECRET
-- redirect_uri_path				OID_REDIRECT
-- discovery					OID_DISCOVERY
-- token_endpoint_auth_method			OID_AUTH_METHOD
-- logout_path					OID_LOGOUT_PATH
-- redirect_after_logout_with_id_token_hint	OID_LOGOUT_HINT
-- revoke_tokens_on_logout			OID_LOGOUT_REVOKE_TOKENS
-- renew_access_token_on_expiry			OID_RENEW_ACCESS_TOKEN_ON_EXPIRY
-- refresh_session_interval			OID_REFRESH_SESSION_INTERVAL
-- access_token_expires_leeway			OID_ACCESS_TOKEN_EXPIRES_LEEWAY
-- iat_slack					OID_IAT_SLACK
-- accept_none_alg				OID_ACCEPT_NONE_ALG
-- accept_unsupported_alg			OID_ACCEPT_UNSUPPORTED_ALG
-- use_nonce					OID_USE_NONCE
-- use_pkce					OID_USE_PKCE
-- response_mode				OID_RESPONSE_MODE
--
local opts = {
	-- Proxy credentials
	--
	client_id     = os.getenv("OID_CLIENT_ID"),
	client_secret = os.getenv("OID_CLIENT_SECRET"),

	-- AAA related options
	--
	redirect_uri_path            = utils.getEnv("OID_REDIRECT", def.redirect),
	discovery                    = utils.getEnv("OID_DISCOVERY", def.discovery),
    	token_endpoint_auth_method   = utils.getEnv("OID_AUTH_METHOD", def.authMethod),
	logout_path                  = utils.getEnv("OID_LOGOUT_PATH", def.logoutPath),
	redirect_after_logout_with_id_token_hint = utils.getEnv("OID_LOGOUT_HINT", def.logoutHint),
	revoke_tokens_on_logout      = utils.getEnv("OID_LOGOUT_REVOKE_TOKENS", def.logoutRevokeTokens),
	renew_access_token_on_expiry = utils.getEnv("OID_RENEW_ACCESS_TOKEN_ON_EXPIRY", def.renewAccessTokensOnExpiry),
	refresh_session_interval     = utils.getEnv("OID_REFRESH_SESSION_INTERVAL", def.refreshSessionInterval),
	access_token_expires_leeway  = utils.getEnv("OID_ACCESS_TOKEN_EXPIRES_LEEWAY", def.accessTokenExpiresLeeway),

	-- JWT options
	--
	iat_slack              = utils.getEnv("OID_IAT_SLACK", def.iatSlack),
	accept_none_alg        = utils.getEnv("OID_ACCEPT_NONE_ALG", def.acceptNoneAlg),
	accept_unsupported_alg = utils.getEnv("OID_ACCEPT_UNSUPPORTED_ALG", def.acceptUnsupportedAlg),
	use_nonce              = utils.getEnv("OID_USE_NONCE", def.useNonce),
	use_pkce               = utils.getEnv("OID_USE_PKCE", def.usePkce),

	-- handle a "code" authorization response from the OP
	response_mode = utils.getEnv("OID_RESPONSE_MODE", def.responseMode),
}

-- Check that mandatory options are set
--
if opts.client_id == nil or opts.client_secret == nil then
	ngx.log(ngx.INFO, "Client authentication failed.")
	ngx.exit(ngx.INTERNAL_SERVER_ERROR)
end

if ngx.ctx.bearer == false then
	ngx.log(ngx.INFO, "No bearer")
--
-- Main Part - Communicate with AAA and take relevant actions
--

-- 1. Call authenticate for OpenID Connect user authentication

	local res, err, _target, session = require("resty.openidc").authenticate(opts)

	if err then
		ngx.log(ngx.INFO, "Authentication failed: " .. err)
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end

	if res == nil then
		ngx.log(ngx.INFO, "Authentication failed")
		return
	end

-- 2. Verify/Decrypt token
--
-- When token passes verification, the payload will be in the form:
--
-- 	"payload": {
-- 		"session_state":"1c0b3a4a-8f62-4b58-945c-b1591494dee5",
-- 		"email_verified":true,
-- 		"azp":"uc3",
-- 		"given_name":"",
-- 		"iss":"https:\/\/user179.mhl.tuc.gr\/realms\/UC3",
-- 		"sid":"1c0b3a4a-8f62-4b58-945c-b1591494dee5",
-- 		"sub":"0d766811-1bb8-4679-ad4d-65979b324135",
-- 		"auth_time":1664796473,
-- 		"exp":1664796773,
-- 		"family_name":"",
-- 		"preferred_username":"bob",
-- 		"typ":"Bearer",
-- 		"scope":"openid app2 apps profile email app1",
-- 		"nonce":"ed531be5a45adaca87fbbc06b45a7028",
-- 		"jti":"eeec9df7-eeb0-4725-a538-812792e89475",
-- 		"iat":1664796473
-- 	}
--

	local payload, err = require("resty.openidc").jwt_verify(session.data.access_token, opts)

	if err then
		ngx.log(ngx.INFO, "Token verification failed: " .. err)
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end

	ngx.log(ngx.INFO, "Evaluate user **" .. payload["preferred_username"] .. "** with scopes: ", payload["scope"])

-- 3. Check if access is granted based on the user scopes
--
	if utils.accessGranted(aaa.resources, payload["scope"]) == true then
		ngx.log(ngx.INFO, "Authorization success.")

		if opts.token_endpoint_auth_method == "client_secret_basic" then
			ngx.req.set_header("Authorization", "Bearer "..session.data.enc_id_token)
		end
	else
		ngx.log(ngx.INFO, "Access not granted")
		ngx.exit(ngx.HTTP_FORBIDDEN)
	end
else
--
-- Main Part - Communicate with AAA and take relevant actions
--

	-- 1. Verify bearer
	-- https://github.com/zmartzone/lua-resty-openidc/blob/master/lib/resty/openidc.lua#L1848
	local json, err, access_token =  require("resty.openidc").bearer_jwt_verify(opts)

	if json == nil then
		ngx.log(ngx.INFO, "Bearer auth failed")
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end

-- 2. Verify/Decrypt token
--
-- When token passes verification, the payload will be in the form:
--
-- 	"payload": {
-- 		"session_state":"1c0b3a4a-8f62-4b58-945c-b1591494dee5",
-- 		"email_verified":true,
-- 		"azp":"uc3",
-- 		"given_name":"",
-- 		"iss":"https:\/\/user179.mhl.tuc.gr\/realms\/UC3",
-- 		"sid":"1c0b3a4a-8f62-4b58-945c-b1591494dee5",
-- 		"sub":"0d766811-1bb8-4679-ad4d-65979b324135",
-- 		"auth_time":1664796473,
-- 		"exp":1664796773,
-- 		"family_name":"",
-- 		"preferred_username":"bob",
-- 		"typ":"Bearer",
-- 		"scope":"openid app2 apps profile email app1",
-- 		"nonce":"ed531be5a45adaca87fbbc06b45a7028",
-- 		"jti":"eeec9df7-eeb0-4725-a538-812792e89475",
-- 		"iat":1664796473
-- 	}
--

	local payload, err = require("resty.openidc").jwt_verify(access_token, opts)

	if err then
		ngx.log(ngx.INFO, "Token verification failed: " .. err)
		ngx.exit(ngx.HTTP_UNAUTHORIZED)
	end

	ngx.log(ngx.INFO, "Evaluate user **" .. payload["preferred_username"] .. "** with scopes: ", payload["scope"])

-- 3. Check if access is granted based on the user scopes
--
	if utils.accessGranted(aaa.resources, payload["scope"]) == true then
		ngx.log(ngx.INFO, "Authorization success.")

		if opts.token_endpoint_auth_method == "client_secret_basic" then
			ngx.req.set_header("Authorization", "Bearer "..access_token)
		end
	else
		ngx.log(ngx.INFO, "Access not granted")
		ngx.exit(ngx.HTTP_FORBIDDEN)
	end
end

