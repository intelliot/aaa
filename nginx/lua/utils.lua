local utils = {}

-- Return environment variable if it is set or a default value if it is not
--
function utils.getEnv(varName, default)
	local val = os.getenv(varName)

	if val == nil then
		return default
	end

	return val
end

-- Escape special regex characters from input string
--
function utils.escape(scope)
	scope = string.gsub(scope, "%%", "%%%%")
	scope = string.gsub(scope, "%-", "%%-")
	scope = string.gsub(scope, "%?", "%%?")
	scope = string.gsub(scope, "%+", "%%+")
	scope = string.gsub(scope, "%*", "%%*")
	scope = string.gsub(scope, "%(", "%%(")
	scope = string.gsub(scope, "%)", "%%)")
	scope = string.gsub(scope, "%.", "%%.")
	scope = string.gsub(scope, "%[", "%%[")
	scope = string.gsub(scope, "%^", "%%^")
	scope = string.gsub(scope, "%$", "%%$")
	return scope
end

-- Returns scope from request_uri
--
-- If the request uri begins with a known resource, it returns that resource name.
-- If no known resources match, it returns "/".
--
function utils.getResource(scopes)
	local req = ngx.var.request_uri
	ngx.log(ngx.DEBUG, "[getResource] scopes = ", scopes)
	ngx.log(ngx.DEBUG, "[getResource] request_uri = ", req)

	for scope in scopes:gmatch("%S+") do
		escaped = utils.escape(scope)

		-- Check if resource is in the form '/app1' or '/app1/'
		a = string.match(req, "^/"..escaped.."/?$")
		ngx.log(ngx.DEBUG, "[getResource] check scope(1): ", a)
		if a ~= nil then
			return scope
		end

		-- Check if resource is in the form '/app1/...'
		b = string.match(req, "^/"..escaped.."/.*$")
		ngx.log(ngx.DEBUG, "[getResource] check scope(2): ", b)
		if b ~= nil then
			return scope
		end
	end

	-- Unknown scopes are handled by 'edge-proxy'
	ngx.log(ngx.WARN, "[getResource] unknown scope for request: ", req)
	ngx.log(ngx.INFO, "[getResource] returning default scope '/'")
	return "/"
end

-- Return if the user can access the requested resource using
-- the allowed scopes defined in the JWT
--
function utils.accessGranted(all_scopes, authorized_scopes)
	-- The resource is the backend service requested from the user
	local resource = utils.getResource(all_scopes)
	ngx.log(ngx.DEBUG, "[accessGranted] resource: ", resource)

	-- Only authorized users can access the 'edge-proxy' directly
	if resource == "/" then
		for scope in authorized_scopes:gmatch("%S+") do
			ngx.log(ngx.DEBUG, "[accessGranted] scope(admin): ", scope)
			if scope == "proxyadmin" then
				ngx.log(ngx.WARN, "***Admin access***")
				return true
			end
		end

		ngx.log(ngx.DEBUG, "[accessGranted] (admin): failed")
		return false
	end

	-- Access is granted only when the requested resource is part of the JWT
	-- scopes. Of course this can be further fine grained, e.g. by also using
	-- groups.
	for scope in authorized_scopes:gmatch("%S+") do
		ngx.log(ngx.DEBUG, "[accessGranted] scope: ", scope)
		if scope == resource then
			return true
		end
	end

	ngx.log(ngx.DEBUG, "[accessGranted] failed")
	return false
end

return utils
